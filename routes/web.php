<?php

use Illuminate\Support\Facades\Route;
Use App\Http\Controllers\masterbarangController;
Use App\Http\Controllers\mastercustomerController;
Use App\Http\Controllers\mastersupplierController;
Use App\Http\Controllers\purchaseOrderController;
Use App\Http\Controllers\salesOrderController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [masterbarangController::class, 'index']);
Route::get('/barang', [masterbarangController::class, 'index']);
Route::get('/barang/create', [masterbarangController::class, 'create']);
Route::post('/barang/store', [masterbarangController::class, 'store']);
Route::get('/barang/show/{id}', [masterbarangController::class, 'show']);
Route::post('/barang/update/{id}', [masterbarangController::class, 'update']);
Route::get('/destroy/{id}', [masterbarangController::class, 'destroy']);

Route::get('/customer', [mastercustomerController::class, 'index']);
Route::get('/customer/create', [mastercustomerController::class, 'create']);
Route::post('/customer/store', [mastercustomerController::class, 'store']);
Route::get('/customer/show/{id}', [mastercustomerController::class, 'show']);
Route::post('/customer/update/{id}', [mastercustomerController::class, 'update']);
Route::get('/customer/destroy/{id}', [mastercustomerController::class, 'destroy']);

Route::get('/supplier', [mastersupplierController::class, 'index']);
Route::get('/supplier/create', [mastersupplierController::class, 'create']);
Route::post('/supplier/store', [mastersupplierController::class, 'store']);
Route::get('/supplier/show/{id}', [mastersupplierController::class, 'show']);
Route::post('/supplier/update/{id}', [mastersupplierController::class, 'update']);
Route::get('/supplier/destroy/{id}', [mastersupplierController::class, 'destroy']);

Route::get('/purchase-order', [purchaseOrderController::class, 'index']);
Route::get('/purchase-order/create', [purchaseOrderController::class, 'create']);
Route::post('/purchase-order/store', [purchaseOrderController::class, 'store']);
Route::get('/purchase-order/show/{id}', [purchaseOrderController::class, 'show']);
Route::post('/purchase-order/update/{id}', [purchaseOrderController::class, 'update']);
Route::get('/purchase-order/destroy/{id}', [purchaseOrderController::class, 'destroy']);


Route::get('/sales-order', [salesOrderController::class, 'index']);
Route::get('/sales-order/create', [salesOrderController::class, 'create']);
Route::post('/sales-order/store', [salesOrderController::class, 'store']);
Route::get('/sales-order/show/{id}', [salesOrderController::class, 'show']);
Route::post('/sales-order/update/{id}', [salesOrderController::class, 'update']);
Route::get('/sales-order/destroy/{id}', [salesOrderController::class, 'destroy']);
