@extends('layout.main')
@section('container')
  <h1>Purchase Order</h1>
  <br>
  <table class="table table-hover">
      <thead>
        <tr>
          <th scope="col">No</th>
          <th scope="col">Code</th>
          <th scope="col">Barang Id</th>
          <th scope="col">Sales Id</th>
          <th scope="col">Customer ID</th>
          <th scope="col">Qty</th>
          <th scope="col">Total Harga </th>
          <th scope="col">Purchase Date</th>
          <th scope="col">Action</th>
        </tr>
      </thead>
      <tbody>
        @php
        $i= 1;
        @endphp
        @foreach ($purchase_order as $prc)
        <tr>
          <th scope="row">{{ $i++; }}</th>
          <td>{{ $prc->code; }}</td>
          <td>{{ $prc->masterbarang_id; }}</td>
          <td>{{ $prc->supplier_id; }}</td>
          <td>{{ $prc->customer_id;}}</td>
          <td>{{ $prc->qty; }}</td>
          <td>{{ $prc->total_harga; }}</td>
          <td>{{ $prc->tanggal_purchase; }}</td>
          <td>
            <a href="{{url('/purchase-order/show/' .$prc->id)}}" class="btn btn-warning btn-sm">Edit</a>   |
            <a href="{{url('/purchase-order/destroy/' .$prc->id)}}" onclick="return confirm('Are you sure delete this item?')" class="btn btn-danger btn-sm">Delete</a>
          </td>
        </tr>
            @endforeach
      </tbody>
  </table>

@endsection
