@extends('layout.main')
@section('container')
    <h1>Data Customer</h1>

    <br>
    <table class="table table-hover">
        <thead>
          <tr>
            <th scope="col">No</th>
            <th scope="col">Nama</th>
            <th scope="col">Email</th>
            <th scope="col">Actiom</th>
          </tr>
        </thead>
        <tbody>
          @php
          $i= 1;
          @endphp
          @foreach ($customer as $cus)
          <tr>
            <th scope="row">{{$i++;}}</th>
            <td>{{$cus->nama;}}</td>
            <td>{{$cus->email;}}</td>
            <td>
              <a href="{{url('/show/' .$cus->id)}}" class="btn btn-warning btn-sm">Edit</a>   |
              <a href="{{url('/customer/destroy/' .$cus->id)}}" onclick="return confirm('Are you sure delete this item?')"  class="btn btn-danger btn-sm">Delete</a>
            </td>
          </tr>
          @endforeach
        </tbody>
    </table>
@endsection
