@extends('layout.main')
@section('container')
  <h1>ini adalah  halaman  suplier</h1>
  <br>
  <table class="table table-hover">
      <thead>
        <tr>
          <th scope="col">No</th>
          <th scope="col">Nama</th>
          <th scope="col">Email</th>
          <th scope="col">Actiom</th>
        </tr>
      </thead>
      <tbody>
        @php
        $i= 1;
        @endphp
        @foreach ($supplier as $supp)
        <tr>
          <th scope="row">{{$i++;}}</th>
          <td>{{$supp->nama;}}</td>
          <td>{{$supp->email;}}</td>
          <td>
            <a href="{{url('/show/' .$supp->id)}}" class="btn btn-warning btn-sm">Edit</a>   |
            <a href="{{url('/supplier/destroy/' .$supp->id)}}" onclick="return confirm('Are you sure delete this item?')" class="btn btn-danger btn-sm">Delete</a>
          </td>
        </tr>
        @endforeach
      </tbody>
  </table>
@endsection
