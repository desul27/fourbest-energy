@extends('layout.main')
@section('container')
<h1>Data Barang</h1>
  <table class="table table-hover">
    <h1><a href="{{url('/barang/create')}}" class="btn btn-primary btn-sm">+ INSERT</a></h1>
    <p style="float:right;"> {{ "Jumlah Barang: " .$totalBarang }}</p>
      <thead>
        <tr>
          <th scope="col">No</th>
          <th scope="col">Code</th>
          <th scope="col">Nama Barang</th>
          <th scope="col">Stock</th>
          <th scope="col">Harga Satuan</th>
          <th scope="col">Action</th>
        </tr>
      </thead>
      <tbody>
        @php
        if (empty($_GET['page']))
        $i=1;
        else
        $i = ($_GET['page'] * $barang->count()) - ($barang->count()-1);
        @endphp
        @foreach ($barang as $bar)
        <tr>
          <th scope="row">{{ $i++; }}</th>
          <td>{{$bar->code  }}</td>
          <td>{{$bar->nama  }}</td>
          <td>{{$bar->stock  }}</td>
          <td>{{$bar->harga  }}</td>
          <td>
            <a href="{{url('/show/' .$bar->id)}}" class="btn btn-warning btn-sm"> Edit</a>   |
            <a href="{{url('/destroy/' .$bar->id)}}" onclick="return confirm('Are you sure delete this item?')" class="btn btn-danger btn-sm">Delete</a>
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>
<p>{{ $barang->links();}} </p>
@endsection
