@extends('layout.main')
@section('container')
<h1>Insert Barang</h1>
<!-- Main content form -->
      <div class="card card-primary">
      <div class="card-header">
            @if ($errors->any())
                  <div class="alert alert-danger">
                        <ul>
                              @foreach ($errors->all() as $error)
                                    <li> {{ $error }}</li>
                              @endforeach
                        </ul>
                  </div>
            @endif
            @if (session()->has('message'))
                  <div class="alert alert-success">
                        {{ session()->get('message') }}
                  </div>
            @endif
      </div>
      <!-- form start -->
        <form action="{{url('/barang/store')}}" method="post">
          @csrf
          <div class="card-body">
            {{--  Code --}}
            <div class="form-group">
              <label for="code">Kode Produk</label>
              <input type="text" class="form-control" id="code" placeholder="enter code product" name="code" autocomplete="off" autofocus>
            </div>
            {{--  Nama Barang --}}
            <div class="form-group">
              <label for="name">Nama Barang</label>
              <input type="text" class="form-control" id="name" placeholder="enter product name" name="nama" autocomplete="off">
            </div>
            {{-- Stock --}}
            <div class="form-group">
              <label for="stock">Stock </label>
              <input type="integer" class="form-control" id="stock" placeholder="enter stock product"
               name="stock"  autocomplete="off" >
            </div>
            {{-- Harga --}}
            <div class="form-group">
              <label for="harga">Harga </label>
              <input type="text" class="form-control" id="harga" placeholder="enter varian product" name="harga"autocomplete="off" >
            </div>
            {{-- Description --}}
            <div class="form-group">
              <label for="desc">Deskripsi </label>
              <input type="textarea" class="form-control" id="desc" placeholder="enter product description" name="description"  autocomplete="off">
            </div>
          </div>
          <!-- /.card-body -->
          <div class="card-footer">
            <button type="submit" name="submit" class=" btn btn-primary">Submit</button>
          </div>
        </form>
        </div>
<!-- /.Main content form -->
<br>
  <a href="{{url('/barang')}}">Kembali</a>
@endsection
