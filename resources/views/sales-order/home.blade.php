@extends('layout.main')
@section('container')
    <h1>ini adalah  halaman  sales order</h1>
    <table class="table table-hover">
        <thead>
          <tr>
            <th scope="col">No</th>
            <th scope="col">Nama</th>
            <th scope="col">Id Customer</th>
            <th scope="col">Id Barang</th>
            <th scope="col">Id Supplier</th>
            <th scope="col">Qty</th>
            <th scope="col">Total Harga</th>
            <th scope="col">Tanggal Order</th>

            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
          @php
          $i= 1;
          @endphp
          @foreach ($sales_order as $sor)
          <tr>
            <th scope="row">{{ $i++ }}</th>
            <td>{{$sor->nama }}</td>
            <td>{{$sor->customer_id }}</td>
            <td>{{$sor->masterbarang_id  }}</td>
            <td>{{$sor->supplier_id }}</td>
            <td>{{$sor->qty }}</td>
            <td>{{$sor->total_harga }}</td>
            <td>{{$sor->tanggal_order }}</td>

            <td>
              <a  href="{{url('/sales-order/show/' .$sor->id)}}"class="btn btn-warning btn-sm"> Edit</a>   |
              <a href="{{url('/sales-order/destroy/' .$sor->id)}}" onclick="return confirm('Are you sure delete this item?')" class="btn btn-danger btn-sm">Delete</a>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
@endsection
