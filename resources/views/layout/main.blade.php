@include('layout.header')

<div class="container mt-4">
  @yield('container')
</div>

@include('layout.footer')
