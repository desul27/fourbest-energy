<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Faker\Factory as Faker;

class Master_barangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    $faker = Faker::create();
    foreach(range(1,20) as $index) {
      DB::table('tb_masterbarang')->insert([
        'code' => $faker->numberBetween(),
        'nama' => $faker->firstName(),
        'stock' => $faker->numberBetween(1, 199),
        'harga' => $faker->numberBetween(1, 199),


        'description' => $faker->paragraph(10)

      ]);
  }



    }
}
