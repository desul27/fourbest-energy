<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Faker\Factory as Faker;

class Purchase_orderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $faker = Faker::create();
      foreach(range(1,10) as $index) {
       DB::table('tb_purchaseorder')->insert([
      'code' => $faker->numberBetween(),
      'masterbarang_id' => $faker->numberBetween(1, 10),
      'supplier_id' => $faker->numberBetween(1, 10),
      'sales_id' => $faker->numberBetween(1, 10),
      'customer_id' => $faker->numberBetween(1, 5),
      'qty' => $faker->numberBetween(1, 5),
      'total_harga' => $faker->numberBetween(1, 1000),
      'qty' => $faker->numberBetween(1, 5),
      'total_harga' => $faker->numberBetween(1, 10),
      'tanggal_purchase' =>$faker->date(),

    ]);
 }
    }
}
