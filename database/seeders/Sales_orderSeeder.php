<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Faker\Factory as Faker;


class Sales_orderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $faker = Faker::create();
      foreach(range(1,10) as $index) {
       DB::table('tb_sales_order')->insert([
      'nama' => $faker->name(),
      'customer_id' => $faker->numberBetween(1, 10),
      'masterbarang_id' => $faker->numberBetween(1, 10),
      'supplier_id' => $faker->numberBetween(1, 10),
      'qty' => $faker->numberBetween(1, 10),
      'total_harga' => $faker->numberBetween(1, 1000),
      'tanggal_order' =>$faker->date(),
      'email' => $faker->email()
    ]);
 }


    }
}
