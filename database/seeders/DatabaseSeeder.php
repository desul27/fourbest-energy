<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;


use App\Models\Masterbarang;
use App\Models\Mastercustomer;
use App\Models\Mastersupplier;
use App\Models\Purchase_order;
use App\Models\Sales_order;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
      $this->call([
      Master_barangSeeder::class,
      Master_customerSeeder::class,
      Master_supplierSeeder::class,
      Purchase_orderSeeder::class,
      Sales_orderSeeder::class,
      ]);
    }
  }
