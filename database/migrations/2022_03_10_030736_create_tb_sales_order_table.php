<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbSalesOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_sales_order', function (Blueprint $table) {
            $table->id();
            $table->string('nama');
            $table->foreignId('customer_id');
            $table->foreignId('masterbarang_id');
            $table->foreignId('supplier_id');
            $table->integer('qty');
            $table->integer('total_harga');
            $table->date('tanggal_order');
            $table->string('email')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_sales_order');
    }
}
