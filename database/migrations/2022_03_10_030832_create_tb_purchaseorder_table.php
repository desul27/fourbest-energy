<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbPurchaseorderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_purchaseorder', function (Blueprint $table) {
            $table->id();
            $table->string('code')->unique();
            $table->foreignId('masterbarang_id');
            $table->foreignId('supplier_id');
            $table->foreignId('sales_id');
            $table->foreignId('customer_id');
            $table->integer('qty');
            $table->integer('total_harga');
            $table->date('tanggal_purchase');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_purchaseorder');
    }
}
